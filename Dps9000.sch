EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Monitor-Dps9000-rescue:Apds-9930-Library-cbas U1
U 1 1 6103DC10
P 4700 3500
AR Path="/6103DC10" Ref="U1"  Part="1" 
AR Path="/606727AA/6103DC10" Ref="U?"  Part="1" 
F 0 "U1" H 4700 3625 50  0000 C CNN
F 1 "Apds-9930" H 4700 3534 50  0000 C CNN
F 2 "Monitor-RF_MODULE:apds-9930-qga_8" H 4750 3450 50  0001 C CNN
F 3 "" H 4750 3450 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6103DC16
P 5650 3900
AR Path="/606727AA/6103DC16" Ref="#PWR?"  Part="1" 
AR Path="/6103DC16" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 5650 3650 50  0001 C CNN
F 1 "GND" H 5655 3727 50  0000 C CNN
F 2 "" H 5650 3900 50  0001 C CNN
F 3 "" H 5650 3900 50  0001 C CNN
	1    5650 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 3550 5100 3600
Wire Wire Line
	5650 3850 5650 3900
Text GLabel 6050 3550 2    50   Input ~ 0
Vcc
Wire Wire Line
	5100 3800 5400 3800
Wire Wire Line
	5400 3800 5400 3850
Wire Wire Line
	5400 3850 5650 3850
Text GLabel 4150 3600 0    50   Input ~ 0
I2C_DA
Text GLabel 5250 3700 2    50   Input ~ 0
I2C_CL
NoConn ~ 5100 3900
NoConn ~ 4300 3900
NoConn ~ 4300 3800
NoConn ~ 4300 3700
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 6103DC33
P 6750 3750
AR Path="/606727AA/6103DC33" Ref="J?"  Part="1" 
AR Path="/6103DC33" Ref="J1"  Part="1" 
F 0 "J1" H 6858 4131 50  0000 C CNN
F 1 "Conn_01x04_Male" H 6858 4040 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 6750 3750 50  0001 C CNN
F 3 "~" H 6750 3750 50  0001 C CNN
	1    6750 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 6103DC39
P 6950 3950
AR Path="/606727AA/6103DC39" Ref="#PWR?"  Part="1" 
AR Path="/6103DC39" Ref="#PWR0102"  Part="1" 
F 0 "#PWR0102" H 6950 3700 50  0001 C CNN
F 1 "GND" H 6955 3777 50  0000 C CNN
F 2 "" H 6950 3950 50  0001 C CNN
F 3 "" H 6950 3950 50  0001 C CNN
	1    6950 3950
	1    0    0    -1  
$EndComp
Text GLabel 6950 3850 2    50   Input ~ 0
Vcc
Text GLabel 6950 3650 2    50   Input ~ 0
I2C_CL
Text GLabel 6950 3750 2    50   Input ~ 0
I2C_DA
Wire Wire Line
	5100 3550 6050 3550
Wire Wire Line
	5100 3700 5250 3700
Wire Wire Line
	4150 3600 4300 3600
$EndSCHEMATC
